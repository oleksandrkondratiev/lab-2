package edu.hneu.mjt.kondratievoleksandr.bankapi;

import edu.hneu.mjt.kondratievoleksandr.dto.BankCard;
import edu.hneu.mjt.kondratievoleksandr.dto.BankCardType;
import edu.hneu.mjt.kondratievoleksandr.dto.User;

public interface Bank {
    BankCard createBankCard(User user, BankCardType bankCardType);
}
