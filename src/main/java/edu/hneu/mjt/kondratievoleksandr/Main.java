package edu.hneu.mjt.kondratievoleksandr;

import edu.hneu.mjt.kondratievoleksandr.bankapi.Bank;
import edu.hneu.mjt.kondratievoleksandr.cloudbankimpl.CloudBankImpl;
import edu.hneu.mjt.kondratievoleksandr.cloudserviceimpl.CloudServiceImpl;
import edu.hneu.mjt.kondratievoleksandr.dto.BankCardType;
import edu.hneu.mjt.kondratievoleksandr.dto.Subscription;
import edu.hneu.mjt.kondratievoleksandr.dto.User;
import edu.hneu.mjt.kondratievoleksandr.serviceapi.Service;

import java.time.LocalDate;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        // Створюємо екземпляр банку та сервісу
        Bank bank = new CloudBankImpl();
        Service service = new CloudServiceImpl();

        // Створюємо декілька користувачів
        var user1 = new User("Владислав", "Шевченко", LocalDate.of(1996, 2, 22));
        var user2 = new User("Анастасія", "Пивоварова", LocalDate.of(2002, 11, 4));
        var user3 = new User("Іван", "Лукін", LocalDate.of(2000, 5, 2));
        // Створюємо картки для користувачів
        var card1 = bank.createBankCard(user1, BankCardType.CREDIT);
        var card2 = bank.createBankCard(user2, BankCardType.DEBIT);
        var card3 = bank.createBankCard(user3, BankCardType.DEBIT);
        // Підписуємо користувачів на сервіс за допомогою їхніх карток
        service.subscribe(card1, LocalDate.now());
        service.subscribe(card2, LocalDate.now());
        service.subscribe(card3, LocalDate.now());
        var allCards = service.getAllCards();
        allCards.forEach(card -> {
            System.out.println("Прізвище: " + card.getUser().getSurname() + ",\nІм'я: " + card.getUser().getName() +
                    ",\nДата народження: " + card.getUser().getBirthday() + ",\nНомер карти: " + card.getNumber() +
                    ",\nБаланс карти: " + card.getBalance() + " ₴ ,\nКредитний ліміт: " + card.getCreditLimit() + " ₴\n");
        });
        // Отримуємо підписку за номером картки
        Optional<Subscription> subscription1 = service.getSubscriptionByBankCardNumber(card1.getNumber());
        Optional<Subscription> subscription2 = service.getSubscriptionByBankCardNumber(card2.getNumber());
        //Виводимо інформацію про підписки
        subscription1.ifPresent(sub -> System.out.println("\nПідписка 1:\nномер карти: " + sub.getBankcard() +
                ",\nчас реєстрації: " + sub.getStartDate()));
        subscription2.ifPresent(sub -> System.out.println("\nПідписка 2:\nномер карти: " + sub.getBankcard() +
                ",\nчас реєстрації: " + sub.getStartDate()));
    }
}
