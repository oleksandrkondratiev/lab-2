module lab {
    requires jmp.cloud.service.impl;
    requires jmp.dto;
    requires jmp.cloud.bank.impl;
    uses edu.hneu.mjt.kondratievoleksandr.serviceapi.Service;
    uses edu.hneu.mjt.kondratievoleksandr.bankapi.Bank;
}
