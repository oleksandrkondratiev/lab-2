package edu.hneu.mjt.kondratievoleksandr.dto;

public class DebitBankCard extends BankCard {
    public DebitBankCard(User user, String cardNumber, double balance, double creditLimit)
    {
        super(user, cardNumber, balance, creditLimit);
    }
}
