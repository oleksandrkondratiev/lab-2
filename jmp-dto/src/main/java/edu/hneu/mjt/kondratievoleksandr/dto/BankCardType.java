package edu.hneu.mjt.kondratievoleksandr.dto;

public enum BankCardType {
    CREDIT,
    DEBIT
}
