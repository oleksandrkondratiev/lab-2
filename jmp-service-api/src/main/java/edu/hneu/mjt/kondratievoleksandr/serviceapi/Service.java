package edu.hneu.mjt.kondratievoleksandr.serviceapi;

import edu.hneu.mjt.kondratievoleksandr.dto.BankCard;
import edu.hneu.mjt.kondratievoleksandr.dto.Subscription;
import edu.hneu.mjt.kondratievoleksandr.dto.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface Service {
    void subscribe(BankCard bankCard, LocalDate startDate);

    Optional<Subscription> getSubscriptionByBankCardNumber(String bankCardNumber);

    List<User> getAllUsers();
    List<BankCard> getAllCards();
}
