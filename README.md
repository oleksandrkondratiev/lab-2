## Лабораторна робота 2

### Тема: "ДОСЛІДЖЕННЯ ДОДАТКОВОГО ФУНКЦІОНАЛУ В JAVA 8-17"

#### Виконав: Кондратьєв Олександр

## 1) Створити maven проекту з 5-ю модулями:
1. **jmp-bank-api**
2. **jmp-service-api**
3. **jmp-cloud-bank-impl**
4. **jmp-dto**
5. **jmp-cloud-service-impl**

![Скріншот1](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/1.png?ref_type=heads)
**Створив проєкт**
![Скріншот2](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/2.png?ref_type=heads)
![Скріншот3](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/3.png?ref_type=heads)
![Скріншот4](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/4.png?ref_type=heads)
![Скріншот5](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/5.png?ref_type=heads)
![Скріншот6](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/6.png?ref_type=heads)
**Створив модулі**
---
## 2) Створіть наступні класи в модулі jmp-dto:
1. [User]  
   String name;  
   String surmane;  
   LocalDate birthday;
2. [BankCard]  
   String number;  
   User user;
3. [Subscription]  
   String bankcard;  
   LocaleDate startDate;

![Скріншот7](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/7.png?ref_type=heads)
![Скріншот8](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/8.png?ref_type=heads)
![Скріншот9](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/9.png?ref_type=heads)
![Скріншот10](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/10.png?ref_type=heads)
![Скріншот11](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/11.png?ref_type=heads)
**Створив класи в dto**
---
## 3) Подовжте клас BankCard за допомогою:
CreditBankCard  
DebitBankCard

![Скріншот12](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/12.png?ref_type=heads)
![Скріншот13](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/13.png?ref_type=heads)
**Подовжив клас BankCard**
---
## 4) Створити enum :
[BankCardType]  
CREDIT  
DEBIT

![Скріншот14](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/14.png?ref_type=heads)
**Створив Enum**
---
## 5) Додайте інтерфейс Bank до jmp-bank-api за допомогою:
BankCard createBankCard(User, BankCardType)

![Скріншот15](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/15.png?ref_type=heads)
**Створив інтерфейс в заданому модулі**
---
## 6) Додайте module-info.java за допомогою:
requires jmp-dto  
export Bank interface

![Скріншот16](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/16.png?ref_type=heads)
**Створив module-info в модулі jmp-bank-api**
---
## 7) Впровадити Bank в jmp-cloud-bank-impl. Метод повинен створювати новий клас в залежності від типу

![Скріншот17](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/17.png?ref_type=heads)
**Створення відповідної карти за типом**
---

## 8) Додайте module-info.java який містить:
requires transitive module with Bank interface  
requires jmp-dto  
export implementation of Bank interface

![Скріншот18](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/18.png?ref_type=heads)
**Створив module-info в модулі jmp-cloud-bank-impl**
---

## 9) Додайте інтерфейс Service до jmp-service-api за допомогою:
void subscribe(BankCard);  
Optional<Subscription> getSubscriptionByBankCardNumber(String);  
List<User> getAllUsers();

![Скріншот19](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/19.png?ref_type=heads)
**Створив інтерфейс в відповідному модулі**
---

## 10) Додайте module-info.java за допомогою:
requires jmp-dto  
export Services interface

![Скріншот20](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/20.png?ref_type=heads)
**Створив module-info в модулі jmp-service-api**
---

## 11) Впровадити Service в jmp-cloud-service-impl. API потоку користувача. Ви можете використовувати Map або Mongo/БД для перегляду даних.

![Скріншот21](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/21.png?ref_type=heads)
![Скріншот22](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/22.png?ref_type=heads)
**Впровадив Service в CloudServiceImpl**
---

## 12) Додайте module-info.java з налаштуваннями:
requires transitive module with Service interface  
requires jmp-dto  
export implementation of Service interfaces

![Скріншот23](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/23.png?ref_type=heads)
**Створив module-info в модулі jmp-cloud-service-impl**
---

## 13) Використовуйте var для визначення локальних змінних, де б це не було застосовано.
## 14) Використовуйте лямбди та функції Java 8, де б це не було застосовано.
## 15) Створити модуль основного додатку.

![Скріншот24](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/24.png?ref_type=heads)
![Скріншот25](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/25.png?ref_type=heads)
**Використання var для локальних змінних, використання лямбда, модуль основного проєкту**
---

## 16) Додайте module-info.java з налаштуваннями:
use interface  
requires module with Bank implementation  
requires module with Service implementation  
requires jmp-dto

![Скріншот26](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/26.png?ref_type=heads)
**Створив module-info в головному модулі**
---

## 17) Приклад роботи в консолі

![Скріншот27](https://gitlab.com/oleksandrkondratiev/lab-2/-/raw/main/images/27.png?ref_type=heads)
---
